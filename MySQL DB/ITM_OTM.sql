DELIMITER $$
CREATE PROCEDURE ITM_OTM()
BEGIN
    DECLARE finished INTEGER DEFAULT 0;
    DECLARE data_count INTEGER DEFAULT 0;

    -- Signal Table
    DECLARE SignalID INTEGER DEFAULT 0;
    DECLARE SignalPrecioEntrada FLOAT DEFAULT 0;
    DECLARE SignalExpiracion VARCHAR(30) DEFAULT '';
    DECLARE SignalDireccion VARCHAR(5) DEFAULT '';

    -- Data Table
    DECLARE PrecioClose FLOAT DEFAULT 0;

    DECLARE cursor_estatus CURSOR FOR 
    select iSignal,PrecioEntrada,Expiracion,Direccion from bo.signals where DATE(Fecha) = DATE(NOW()) and Estatus = 'In Progress';

    -- declare NOT FOUND handler
    DECLARE CONTINUE HANDLER 
    FOR NOT FOUND SET finished = 1;

    OPEN cursor_estatus;

    get_estatus: LOOP
    FETCH cursor_estatus INTO SignalID,SignalPrecioEntrada,SignalExpiracion,SignalDireccion;
    IF finished = 1 THEN 
        LEAVE get_estatus;
    END IF;

    -- Search for result of signal
    select count(Close) into data_count from forex.data where Fecha = SignalExpiracion;
    SELECT concat('iSignal: ',SignalID,' data_count ', data_count,' PrecioEntrada: ',SignalPrecioEntrada,' Expiracion: ',SignalExpiracion);
    IF(data_count>0) THEN
        select Close into PrecioClose from forex.data where Fecha = SignalExpiracion;
        IF(SignalDireccion = 'UP') THEN
            IF(SignalPrecioEntrada < PrecioClose) THEN
                update bo.signals set Estatus = 'ITM', PrecioExpiracion = PrecioClose where iSignal = SignalID;
            ELSE
                update bo.signals set Estatus = 'OTM', PrecioExpiracion = PrecioClose where iSignal = SignalID;
            END IF;
        END IF;

        IF(SignalDireccion = 'DOWN') THEN
            IF(SignalPrecioEntrada > PrecioClose) THEN
                update bo.signals set Estatus = 'ITM', PrecioExpiracion = PrecioClose where iSignal = SignalID;
            ELSE
                update bo.signals set Estatus = 'OTM', PrecioExpiracion = PrecioClose where iSignal = SignalID;
            END IF;
        END IF;
        
    END IF;

    END LOOP get_estatus;
END $$

DELIMITER ;