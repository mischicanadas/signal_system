//+------------------------------------------------------------------+
//|                                                       Arrows.mq5 |
//|                                       Copyright 2016, Luis Gomez |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//     MySQL Database
//+------------------------------------------------------------------+
#include <MQLMySQL.mqh>
string INI;
string Host, User, Password, Database, Socket; // database credentials
int Port,ClientFlag;
int DB; // database identifier

#property copyright "Copyright 2016, Luis Gomez"
#property link      "https://www.mql5.com"
#property indicator_chart_window 
#property indicator_buffers 6
#property indicator_plots   4
//+----------------------------------------------+
//|  Bearish indicator drawing parameters        |
//+----------------------------------------------+
//---- drawing the indicator 1 as a symbol
#property indicator_type1   DRAW_ARROW
//---- use orange color for the symbol
#property indicator_color1  Tomato
//---- indicator 1 line width is equal to 1
#property indicator_width1  2
//---- displaying the indicator label 1
#property indicator_label1  "Sell Arrow"

//+----------------------------------------------+
//|  Bullish indicator drawing parameters        |
//+----------------------------------------------+
//---- drawing the indicator 2 as a symbol
#property indicator_type2   DRAW_ARROW
//---- chartreuse color is used for the input symbol
#property indicator_color2  MediumSeaGreen
//---- indicator 2 line width is equal to 2
#property indicator_width2  2
//---- displaying the indicator label 2
#property indicator_label2  "Buy Arrow"

//+----------------------------------------------+
//|  Bearish indicator drawing parameters        |
//+----------------------------------------------+
//---- drawing the indicator 3 as a symbol
#property indicator_type3   DRAW_LINE
//---- use orange color for the stop-loss line
#property indicator_color3  Orange
//---- indicator 3 line width is equal to 1
#property indicator_width3  1
//---- displaying the indicator 3 label
#property indicator_label3 "Sell Line"

//+----------------------------------------------+
//|  Bullish indicator drawing parameters        |
//+----------------------------------------------+
//---- drawing the indicator 4 as a symbol
#property indicator_type4   DRAW_LINE
//---- chartreuse color is used for the stop-losses line
#property indicator_color4  Chartreuse
//---- indicator 6 line width is equal to 1
#property indicator_width4  1
//---- displaying the indicator label 4
#property indicator_label4 "Buy Line"

//+----------------------------------------------+
//|  Declaration of enumerations                 |
//+----------------------------------------------+
enum DISPLAY_SIGNALS_MODE // Type of constant
  {
   OnlyStops= 0,          // Only Stops
   SignalsStops,          // Signals & Stops
   OnlySignals            // Only Signals
  };
//+----------------------------------------------+
//|  Indicator input parameters                  |
//+----------------------------------------------+

input bool SoundON=true;                        // Show Alerts
input bool SendToDB=false;                      // Send signal to Database
input bool Line=false;                          // Show Line
bool TurnedUp = false;                          // Validates Alert
bool TurnedDown = false;                        // Validates Alert
int Length=20;                                  // Bollinger Bands period
double Deviation=1;                             // Deviation
double MoneyRisk=1.00;                          // Offset Factor
DISPLAY_SIGNALS_MODE Signal=SignalsStops;       // Display signals mode
//+----------------------------------------------+
//---- declaration of dynamic arrays that
//---- will be used as indicator buffers
double UpTrendBuffer[];
double DownTrendBuffer[];
double UpTrendSignal[];
double DownTrendSignal[];
double UpTrendLine[];
double DownTrendLine[];
//---- declaration of integer variables for the indicators handles
int BB_Handle;
//---- declaration of the integer variables for the start of data calculation
int min_rates_total;
//---- declaration of global variables
double MRisk;
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
void OnInit()
{  

 //+-- MySQL Connection 107.170.200.104
 INI = TerminalInfoString(TERMINAL_PATH)+"\\MQL5\\Scripts\\MyConnection.ini";
 
 // reading database credentials from INI file
 Host = ReadIni(INI, "MYSQL", "Host");
 User = ReadIni(INI, "MYSQL", "User");
 Password = ReadIni(INI, "MYSQL", "Password");
 Database = ReadIni(INI, "MYSQL", "Database");
 Port     = (int)StringToInteger(ReadIni(INI, "MYSQL", "Port"));
 Socket   = ReadIni(INI, "MYSQL", "Socket");
 ClientFlag = (int)StringToInteger(ReadIni(INI, "MYSQL", "ClientFlag")); 
 
//--- initialization of global variables 
   min_rates_total=Length+1;
   MRisk=0.5*(MoneyRisk-1);
//----
   BB_Handle=iBands(NULL,0,Length,0,Deviation,PRICE_CLOSE);
   if(BB_Handle==INVALID_HANDLE) Print(" Failed to get handle of the iBands indicator");

//---- set DownTrendSignal[] dynamic array as an indicator buffer
   SetIndexBuffer(0,DownTrendSignal,INDICATOR_DATA);
//---- shifting the start of drawing of the indicator 1
   PlotIndexSetInteger(0,PLOT_DRAW_BEGIN,min_rates_total);
//---- indicator symbol
   PlotIndexSetInteger(0,PLOT_ARROW,226);
//---- restriction to draw empty values for the indicator
   PlotIndexSetDouble(0,PLOT_EMPTY_VALUE,EMPTY_VALUE);
//---- indexing the elements in the buffer as timeseries
   ArraySetAsSeries(DownTrendSignal,true);

//---- set UpTrendSignal[] dynamic array as an indicator buffer
   SetIndexBuffer(1,UpTrendSignal,INDICATOR_DATA);
//---- shifting the start of drawing the indicator 4
   PlotIndexSetInteger(1,PLOT_DRAW_BEGIN,min_rates_total);
//---- indicator symbol
   PlotIndexSetInteger(1,PLOT_ARROW,225);
//---- restriction to draw empty values for the indicator
   PlotIndexSetDouble(1,PLOT_EMPTY_VALUE,EMPTY_VALUE);
//---- indexing the elements in the buffer as timeseries
   ArraySetAsSeries(UpTrendSignal,true);

//---- set DownTrendLine[] dynamic array as an indicator buffer
   SetIndexBuffer(2,DownTrendLine,INDICATOR_DATA);
//---- shifting the start of drawing the indicator 3
   PlotIndexSetInteger(2,PLOT_DRAW_BEGIN,min_rates_total);
//---- restriction to draw empty values for the indicator
   PlotIndexSetDouble(2,PLOT_EMPTY_VALUE,0);
//---- indexing the elements in the buffer as timeseries
   ArraySetAsSeries(DownTrendLine,true);

//---- set UpTrendLine[] dynamic array as an indicator buffer
   SetIndexBuffer(3,UpTrendLine,INDICATOR_DATA);
//---- shifting the start of drawing the indicator 6
   PlotIndexSetInteger(3,PLOT_DRAW_BEGIN,min_rates_total);
//---- restriction to draw empty values for the indicator
   PlotIndexSetDouble(3,PLOT_EMPTY_VALUE,0);
//---- indexing the elements in the buffer as timeseries
   ArraySetAsSeries(UpTrendLine,true);
   
 
 //---- set DownTrendBuffer[] dynamic array as an indicator buffer
   SetIndexBuffer(4,DownTrendBuffer,INDICATOR_DATA);
//---- shifting the start of drawing the indicator 2
   PlotIndexSetInteger(4,PLOT_DRAW_BEGIN,min_rates_total);
//---- indexing the elements in the buffer as timeseries
   ArraySetAsSeries(DownTrendBuffer,true);
//No necesito mostrar el trailing

//No necesito mostrar el trailing
//---- set UpTrendBuffer[] dynamic array as an indicator buffer
   SetIndexBuffer(5,UpTrendBuffer,INDICATOR_DATA);
//---- shifting the start of drawing the indicator 5
   PlotIndexSetInteger(5,PLOT_DRAW_BEGIN,min_rates_total);
//---- indexing the elements in the buffer as timeseries
   ArraySetAsSeries(UpTrendBuffer,true);
//No necesito mostrar el trailing

//---- setting the format of accuracy of displaying the indicator
   IndicatorSetInteger(INDICATOR_DIGITS,_Digits);
//---- name for the data window and the label for sub-windows 
   string short_name;
   StringConcatenate(short_name,"Arrows (",Length,", ",
                     DoubleToString(Deviation,2),", ",DoubleToString(MoneyRisk,2),")");
   short_name = "Arrows";
   IndicatorSetString(INDICATOR_SHORTNAME,short_name);
//----   
  }
//+------------------------------------------------------------------+
//| Bollinger Bands_Stop_v1                                          |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {

  // To be used for getting recent/latest price quotes
  MqlTick Latest_Price; // Structure to get the latest prices      
  SymbolInfoTick(Symbol() ,Latest_Price); // Assign current prices to structure 

  // The BID price. Is what I see in MT5
  static double dBid_Price; 

  // The ASK price.
  static double dAsk_Price; 

  dBid_Price = Latest_Price.bid;  // Current Bid price.
  dAsk_Price = Latest_Price.ask;  // Current Ask price.

//---- checking the number of bars to be enough for the calculation
   if(BarsCalculated(BB_Handle)<rates_total || rates_total<min_rates_total) return(0);

//---- declarations of local variables 
   int limit,bar,trend,to_copy,maxbar;
   double smax0,smin0,bsmax0,bsmin0;
   double UpBB[],DnBB[],dsize;

//---- memory variables declarations  
   static int trend_;
   static double smax1,smin1,bsmax1,bsmin1;

//---- calculations of the 'limit' starting index for the bars recalculation loop
   if(prev_calculated>rates_total || prev_calculated<=0)// checking for the first start of the indicator calculation
     {
      limit=rates_total-min_rates_total-1; // starting index for calculation of all bars

      smax1=-999999999;
      smin1=+999999999;
      bsmax1=-999999999;
      bsmin1=+999999999;
      trend_=0;

      for(bar=rates_total-1; bar>limit; bar--)
        {
         UpTrendBuffer[bar]=0;
         DownTrendBuffer[bar]=0;
         UpTrendSignal[bar]=EMPTY_VALUE;
         DownTrendSignal[bar]=EMPTY_VALUE;
         UpTrendLine[bar]=0;
         DownTrendLine[bar]=0;
        }
     }
   else
     {
      limit=rates_total-prev_calculated; // starting index for calculation of new bars
     }

//---- indexing elements in arrays as timeseries
   ArraySetAsSeries(UpBB,true);
   ArraySetAsSeries(DnBB,true);
   ArraySetAsSeries(close,true);
   to_copy=limit+1;
   maxbar=rates_total-min_rates_total-1;

//--- copy newly appeared data in the array
   if(CopyBuffer(BB_Handle,1,0,to_copy,UpBB)<=0) return(0);
   if(CopyBuffer(BB_Handle,2,0,to_copy,DnBB)<=0) return(0);

//---- restore values of the variables
   trend=trend_;

//---- main indicator calculation loop
   for(bar=limit; bar>=0 && !IsStopped(); bar--)
     {
      //---- store values of the variables before running at the current bar
      if(rates_total!=prev_calculated && bar==0)
        {
         trend_=trend;
        }

      smax0=UpBB[bar];
      smin0=DnBB[bar];
      UpTrendBuffer[bar]=0;
      DownTrendBuffer[bar]=0;
      UpTrendSignal[bar]=EMPTY_VALUE;
      DownTrendSignal[bar]=EMPTY_VALUE;
      UpTrendLine[bar]=0;
      DownTrendLine[bar]=0;

      if(bar>maxbar)
        {
         smin1=smin0;
         smax1=smax0;
         bsmax1=smax1+MRisk*(smax1-smin1);
         bsmin1=smin1-MRisk*(smax1-smin1);
         continue;
        }

      if(close[bar]>smax1) trend=1;
      if(close[bar]<smin1) trend=-1;

      if(trend>0 && smin0<smin1) smin0=smin1;
      if(trend<0 && smax0>smax1) smax0=smax1;

      dsize=MRisk*(smax0-smin0);
      bsmax0=smax0+dsize;
      bsmin0=smin0-dsize;

      if(trend>0 && bsmin0<bsmin1) bsmin0=bsmin1;
      if(trend<0 && bsmax0>bsmax1) bsmax0=bsmax1;

      if(trend>0)
        {
            if(Signal && !UpTrendBuffer[bar+1])
              {
                  UpTrendSignal[bar]=bsmin0;
                  UpTrendBuffer[bar]=bsmin0;
                  
                  if(Line) UpTrendLine[bar]=bsmin0;
                  if (SoundON==true && bar==0 && !TurnedUp)
                     {
                        if(SendToDB) MySQL_Insert("UP",dBid_Price);
                        Alert("Arrow UP on ",Symbol()," - M",Period());
                        TurnedUp = true;
                        TurnedDown = false;
                     }
              }
             else
              {
                  UpTrendBuffer[bar]=bsmin0;
                  if(Line) UpTrendLine[bar]=bsmin0;
                  UpTrendSignal[bar]=EMPTY_VALUE;
              }
   
            if(Signal==OnlySignals) UpTrendBuffer[bar]=0;
            
            DownTrendSignal[bar]=EMPTY_VALUE;
            DownTrendBuffer[bar]=0;
            DownTrendLine[bar]=0;
        }

      if(trend<0)
        {
            if(Signal && !DownTrendBuffer[bar+1])
              {
                  DownTrendSignal[bar]=bsmax0;
                  DownTrendBuffer[bar]=bsmax0;
                  
                  if(Line) DownTrendLine[bar]=bsmax0;
                  if (SoundON==true && bar==0 && !TurnedDown)
                     {              
                        if(SendToDB) MySQL_Insert("DOWN",dBid_Price);          
                        Alert("Arrow Down on ",Symbol()," - M",Period());
                        TurnedDown = true;
                        TurnedUp = false;
                     }
              }
            else
              {
                  DownTrendBuffer[bar]=bsmax0;
                  if(Line) DownTrendLine[bar]=bsmax0;
                  DownTrendSignal[bar]=EMPTY_VALUE;
              }
            if(Signal==OnlySignals) DownTrendBuffer[bar]=0;
            
            UpTrendSignal[bar]=EMPTY_VALUE;
            UpTrendBuffer[bar]=0;
            UpTrendLine[bar]=0;
        }

      if(bar>0)
        {
         smax1=smax0;
         smin1=smin0;
         bsmax1=bsmax0;
         bsmin1=bsmin0;
        }

     }
 
   return(rates_total);
  }
//+------------------------------------------------------------------+
// Return Full DateTime
//+------------------------------------------------------------------+
string getDateTime(datetime tm)
{
   MqlDateTime stm;
   TimeToStruct(tm,stm);
   
   string Anio = (string)stm.year;
   string Mes = (string)stm.mon;
   string Dia = (string)stm.day;
   string Hora = (string)stm.hour;
   string Minuto = (string)stm.min;
   string Segundo = (string)stm.sec;
   if (stm.mon < 10)
      Mes = "0"+(string)stm.mon;
   if(stm.day < 10)
      Dia = "0"+(string)stm.day;
   if(stm.hour < 10)
      Hora = "0"+(string)stm.hour;
   if(stm.min < 10)
      Minuto = "0"+(string)stm.min;
   if(stm.sec < 10)
      Segundo = "0"+(string)stm.sec;

   //Validacion para solo tomar en cuenta minutos
      Segundo = "00";
      
   return Anio+"-"+Mes+"-"+Dia+" "+Hora+":"+Minuto+":"+Segundo;
}

//+------------------------------------------------------------------+
// Insert into MySQL Database
//+------------------------------------------------------------------+
void MySQL_Insert(string Direccion, double Precio)
{
  datetime tm=TimeLocal();
  datetime ftm = tm + 900;
  
  string Entrada = getDateTime(tm);
  string Expiracion = getDateTime(ftm);

  string Query;
  Query = "insert into signals(Moneda,Direccion,PrecioEntrada,Entrada,Expiracion,TimeFrame,Estatus,Tipo)values('"+_Symbol+"','"+Direccion+"',"+DoubleToString(Precio)+",'"+Entrada+"','"+Expiracion+"','1 Min','In Progress','Signal')";
  Print(Query); 
   
  DB = MySqlConnect(Host, User, Password, Database, Port, Socket, ClientFlag);
  MySqlExecute(DB, Query);                       
  MySqlDisconnect(DB);
}