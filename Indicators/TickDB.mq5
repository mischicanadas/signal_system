//+------------------------------------------------------------------+
//|                                                       TickDB.mq5 |
//|                                       Copyright 2016, Luis Gomez |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//     MySQL Database
//+------------------------------------------------------------------+
#include <MQLMySQL.mqh>
string INI;
string Host, User, Password, Database, Socket; // database credentials
int Port,ClientFlag;
int DB; // database identifier

#property copyright "Copyright 2016, Luis Gomez"
#property link      "https://www.mql5.com"

#property indicator_chart_window 
#property indicator_buffers 1 
#property indicator_plots   1 
//---- plot Line 
#property indicator_label1  "Line" 
#property indicator_type1   DRAW_LINE 
#property indicator_color1  clrDarkBlue 
#property indicator_style1  STYLE_SOLID 
#property indicator_width1  1 
//--- indicator buffers 
double         LineBuffer[]; 
//+------------------------------------------------------------------+ 
//| Custom indicator initialization function                         | 
//+------------------------------------------------------------------+ 
int OnInit() 
{
    //+-- MySQL Connection 107.170.200.104
    INI = TerminalInfoString(TERMINAL_PATH)+"\\MQL5\\Scripts\\MyConnectionForex.ini";
    
    // reading database credentials from INI file
    Host = ReadIni(INI, "MYSQL", "Host");
    User = ReadIni(INI, "MYSQL", "User");
    Password = ReadIni(INI, "MYSQL", "Password");
    Database = ReadIni(INI, "MYSQL", "Database");
    Port     = (int)StringToInteger(ReadIni(INI, "MYSQL", "Port"));
    Socket   = ReadIni(INI, "MYSQL", "Socket");
    ClientFlag = (int)StringToInteger(ReadIni(INI, "MYSQL", "ClientFlag")); 

//--- indicator buffers mapping 
   SetIndexBuffer(0,LineBuffer,INDICATOR_DATA); 
//--- 
   return(INIT_SUCCEEDED); 
} 
//+------------------------------------------------------------------+ 
//| Custom indicator iteration function                              | 
//+------------------------------------------------------------------+ 
int OnCalculate(const int rates_total, 
                const int prev_calculated, 
                const datetime& time[], 
                const double& open[], 
                const double& high[], 
                const double& low[], 
                const double& close[], 
                const long& tick_volume[], 
                const long& volume[], 
                const int& spread[]) 
  { 

    // Info of the last BAR
    //---------------------------------

    // Rates Structure for the data of the Last incomplete BAR
    MqlRates BarData[2]; 
    CopyRates(Symbol(), Period(), 0, 2, BarData); // Copy the data of last incomplete BAR
    
    // Copy latest open price.
    double Latest_Open_Price = BarData[0].open;
    // Copy latest high price.
    double Latest_High_Price = BarData[0].high;
    // Copy latest low price.
    double Latest_Low_Price = BarData[0].low;
    // Copy latest close price.
    double Latest_Close_Price = BarData[0].close;
    // Copy latest volume price.
    //double Latest_Volume_Price = BarData[0].tick_volume;
      
    // Info of the last tick.
    //-----------------------
   
    // To be used for getting recent/latest price quotes
   MqlTick Latest_Price; // Structure to get the latest prices      
   SymbolInfoTick(Symbol() ,Latest_Price); // Assign current prices to structure 

    // The BID price. Is what I see in MT5
    static double dBid_Price; 

    // The ASK price.
    static double dAsk_Price; 

    dBid_Price = Latest_Price.bid;  // Current Bid price.
    dAsk_Price = Latest_Price.ask;  // Current Ask price.
   
//--- Get the number of bars available for the current symbol and chart period 
    int bars=Bars(Symbol(),0);

//--- Get current Bid data time
    datetime btm=TimeLocal();
    string FechaBid = getDateTime(btm,0);

//--- Get previous bar 1 min date time
    datetime tm=TimeLocal()-60;
    string Fecha = getDateTime(tm,1);

   //Alert("Fecha: ",Fecha,"Open = ",DoubleToString(Latest_Open_Price)," Close = ",DoubleToString(Latest_Close_Price)," Bid: "+DoubleToString(dBid_Price));

   //Tick
   MySQL_Tick(dAsk_Price,dBid_Price,FechaBid);

   //Data
   MySQL_Data(Latest_Open_Price, Latest_High_Price, Latest_Low_Price, Latest_Close_Price, Fecha);

//--- return value of prev_calculated for next call 
   return(rates_total); 
  } 
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Return Full DateTime
//+------------------------------------------------------------------+
string getDateTime(datetime tm, int type)
{
   MqlDateTime stm;
   TimeToStruct(tm,stm);
   
   string Anio = (string)stm.year;
   string Mes = (string)stm.mon;
   string Dia = (string)stm.day;
   string Hora = (string)stm.hour;
   string Minuto = (string)stm.min;
   string Segundo = (string)stm.sec;
   if (stm.mon < 10)
      Mes = "0"+(string)stm.mon;
   if(stm.day < 10)
      Dia = "0"+(string)stm.day;
   if(stm.hour < 10)
      Hora = "0"+(string)stm.hour;
   if(stm.min < 10)
      Minuto = "0"+(string)stm.min;

   if(type == 0)
   {
      if(stm.sec < 10)
      Segundo = "0"+(string)stm.sec;
   }
   else
      Segundo = "00";

   return Anio+"-"+Mes+"-"+Dia+" "+Hora+":"+Minuto+":"+Segundo;
}

//+------------------------------------------------------------------+
// Insert into MySQL Database
//+------------------------------------------------------------------+
void MySQL_Tick(double _Ask, double _Bid, string _Fecha)
{
  string Query;
  Query = "replace into tick(Moneda,Ask,Bid,Fecha)values('"+_Symbol+"',"+DoubleToString(_Ask)+","+DoubleToString(_Bid)+",'"+_Fecha+"')";
   
  DB = MySqlConnect(Host, User, Password, Database, Port, Socket, ClientFlag);
  MySqlExecute(DB, Query);                       
  MySqlDisconnect(DB);
}

void MySQL_Data(double _Open, double _High, double _Low, double _Close, string _Fecha)
{
  string Query;
  Query = "replace into data(Moneda,Open,High,Low,Close,Fecha)values('"+_Symbol+"',"+DoubleToString(_Open)+","+DoubleToString(_High)+","+DoubleToString(_Low)+","+DoubleToString(_Close)+",'"+_Fecha+"')";
   
  DB = MySqlConnect(Host, User, Password, Database, Port, Socket, ClientFlag);
  MySqlExecute(DB, Query);                       
  MySqlDisconnect(DB);
}